<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/9/22
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${authUser.username}</title>
    <%@ include file="../layouts/link.html" %>
</head>
<body>
<%@ include file="../layouts/menu.jsp" %>
<div class="container" style="padding-top: 2.6%;">
    Hello ${authUser.username}
</div>

<%@ include file="../layouts/scripts.html" %>
</body>
</html>
