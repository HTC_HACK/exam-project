<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/9/22
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${authUser.username}</title>
    <%@ include file="../layouts/link.html" %>
</head>
<body>
<%@ include file="../layouts/menu.jsp" %>
<div class="container" style="padding-top: 2.6%;">
    <div class="row">
        <div class="col-md-12">
            <a href='/admin/books' class="btn btn-primary"
               style="float: right; margin-bottom: 2rem;margin-top: -1rem">
                <i class="fas fa-arrow-left"></i> BACK</a>
            <br>
            <table class="table table-bordered" style="text-align: center;background: white">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>${book.id}</td>
                    <td>${book.name}</td>
                    <td>${book.description}</td>
                </tr>
                </tbody>
                <tbody></tbody>
                <tbody></tbody>
            </table>

            <table class="table table-bordered" style="text-align: center;background: white">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${authors}" var="author" varStatus="loop">
                    <tr>
                        <td>${loop.count}</td>
                        <td>${author.username}</td>
                    </tr>
                </c:forEach>
                </tbody>
                <tbody></tbody>
                <tbody></tbody>
            </table>
        </div>

    </div>
</div>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<%@ include file="../layouts/scripts.html" %>
</body>
</html>
