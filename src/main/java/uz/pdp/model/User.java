package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/25/22 8:47 AM

public class User {
    private Integer id;
    private String username;
    private String password;
    private Integer role_id;
}
