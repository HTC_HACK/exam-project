package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/25/22 8:55 AM

public class Book {

    private Integer id;
    private String name;
    private String description;
    private String image_path;


}
