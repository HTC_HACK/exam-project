package uz.pdp.dao;


//Asadbek Xalimjonov 2/25/22 9:38 AM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.model.Book;
import uz.pdp.model.User;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookDao {


    @Autowired
    JdbcTemplate template;

    @Autowired
    AuthDao authDao;


    public List<Book> getAllBooks() {
        try {
            String queryStr = "select * from books order by id desc;";
            return template.query(queryStr, (rs, row) -> {
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setName(rs.getString("name"));
                book.setDescription(rs.getString("description"));
                book.setImage_path(rs.getString("image_path"));
                return book;
            });
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }

    public boolean deleteBook(Integer id) {
        try {
            String query = "delete from books where id=" + id + " ;";
            template.update(query);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean saveBook(Book book, List<User> users) {
        try {
            String sql = "insert into books(name,description,image_path) values('" + book.getName() + "','" + book.getDescription() + "','" + book.getImage_path() + "')";
            template.update(sql);

            Integer book_id = template.queryForObject("select max(id) from books;", (rs, row) -> {
                Integer e = rs.getInt(1);
                return e;
            });

            for (User author : users) {
                String sql1 = "insert into book_author(user_id,book_id) values('" + author.getId() + "','" + book_id + "')";
                template.update(sql1);
            }

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Book getBookById(Integer id) {
        try {
            String queryStr = "select * from books where id=" + id + " ;";
            return template.queryForObject(queryStr, (rs, row) -> {
                Book book = new Book();
                book.setName(rs.getString("name"));
                book.setDescription(rs.getString("description"));
                book.setId(rs.getInt("id"));
                book.setImage_path(rs.getString("image_path"));
                return book;
            });
        } catch (Exception e) {
            return null;
        }
    }

    public boolean updateBook(Integer id, Book book, List<User> users) {
        try {


            String sql = "update books set name='" + book.getName() + "' , description='" + book.getDescription() + "' where id=" + id + " ;";

            Integer count = template.queryForObject("select count(book_id) from book_author where book_id=" + id + ";", (rs, row) -> {
                Integer e = rs.getInt(1);
                return e;
            });

            if (count > 0) {
                for (User author : authDao.getBookAuthor(id)) {
                    String sql3 = "delete from book_author where book_id=" + id + " ;";
                    template.update(sql3);
                }
                for (User author : users) {
                    String sql1 = "insert into book_author(user_id,book_id) values('" + author.getId() + "','" + id + "')";
                    template.update(sql1);
                }
            } else {
                for (User author : users) {
                    String sql1 = "insert into book_author(user_id,book_id) values('" + author.getId() + "','" + id + "')";
                    template.update(sql1);
                }
            }

            template.update(sql);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
