package uz.pdp.dao;


//Asadbek Xalimjonov 2/25/22 9:11 AM

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uz.pdp.model.User;

import java.util.List;

@Repository
public class AuthDao {


    @Autowired
    JdbcTemplate template;

    public User getUser(String username, String password) {
        try {
            String queryStr = "select * from users where username='" + username + "' and password='" + password + "' ;";
            return template.queryForObject(queryStr, (rs, row) -> {
                User user = new User();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setRole_id(rs.getInt("role_id"));
                return user;
            });
        } catch (Exception e) {
            return null;
        }
    }

    public List<User> getAuthor() {
        try {
            String queryStr = "select * from users where role_id=2";
            return template.query(queryStr, (rs, row) -> {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setRole_id(rs.getInt("role_id"));
                return user;
            });
        } catch (Exception e) {
            return null;
        }
    }


    public List<User> getBookAuthor(Integer id) {
        try {
            String queryStr = "select u.*\n" +
                    "from book_author\n" +
                    "join users u on u.id = book_author.user_id\n" +
                    "where book_id=" + id + ";";
            return template.query(queryStr, (rs, row) -> {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setRole_id(rs.getInt("role_id"));
                return user;
            });
        } catch (Exception e) {
            return null;
        }
    }
}
