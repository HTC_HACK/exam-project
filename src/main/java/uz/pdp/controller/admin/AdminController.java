package uz.pdp.controller.admin;


//Asadbek Xalimjonov 2/25/22 9:19 AM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.pdp.model.Book;
import uz.pdp.model.User;
import uz.pdp.service.AuthService;
import uz.pdp.service.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {


    @Autowired
    BookService bookService;

    @Autowired
    AuthService authService;


    @GetMapping
    public String getAdminPage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            return "/admin/index";
        }
        return "redirect:/auth/login";
    }


    @GetMapping("/books")
    public String getBooksPage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            model.addAttribute("bookList", bookService.getAllBooks());
            return "admin/book";
        }
        return "redirect:/auth/login";
    }


    @GetMapping("/books/create")
    public String getBookCreatePage(HttpSession session, Model model) {

        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            model.addAttribute("authors", authService.getAuthor());
            return "admin/book-form";
        }
        return "redirect:/auth/login";
    }

    @GetMapping("/books/show/{id}")
    public String showBook(Model model, HttpSession session, @PathVariable Integer id) {

        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            try {
                Book book = bookService.getBookById(id);
                List<User> authors = authService.getBookAuthor(id);
                model.addAttribute("authors", authors);
                model.addAttribute("book", book);
                return "admin/book-show";
            } catch (Exception e) {
                return "redirect:/admin/books";
            }
        }
        return "redirect:/auth/login";
    }

    @PostMapping("/books/create")
    public String saveBook(HttpServletRequest request, HttpSession session) {

        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {

            String name = request.getParameter("name");
            String description = request.getParameter("description");
            String[] authorsId = request.getParameterValues("authors");
            Book book = new Book();
            book.setName(name);
            book.setDescription(description);

            List<User> userList = new ArrayList<>();
            for (int i = 0; i < authorsId.length; i++) {
                User user1 = new User();
                user1.setId(Integer.parseInt(authorsId[i]));
                userList.add(user1);
            }

            bookService.saveBook(book, userList);
            return "redirect:/admin/books";
        }
        return "redirect:/auth/login";
    }

    @GetMapping("/books/edit/{id}")
    public String getBookEditPage(Model model, HttpSession session, @PathVariable Integer id) {

        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            try {
                Book book = bookService.getBookById(id);
                List<User> authors = authService.getBookAuthor(id);
                List<User> author1 = authService.getAuthor();
                author1.removeAll(authors);
                System.out.println(author1);
                model.addAttribute("authors", author1);
                model.addAttribute("authorList", authService.getAuthor());
                model.addAttribute("bookAuthors", authors);
                model.addAttribute("book", book);
                return "admin/book-edit";
            } catch (Exception e) {
                return "redirect:/admin/books";
            }
        }
        return "redirect:/auth/login";
    }

    @PostMapping("/books/edit/{id}")
    public String updateBook(HttpServletRequest request, HttpSession session, @PathVariable Integer id) {

        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            try {
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String[] authorsId = request.getParameterValues("authors");
                Book book = new Book();
                book.setName(name);
                book.setDescription(description);

                List<User> userList = new ArrayList<>();
                for (int i = 0; i < authorsId.length; i++) {
                    User user1 = new User();
                    user1.setId(Integer.parseInt(authorsId[i]));
                    userList.add(user1);
                }

                bookService.updateBook(id, book, userList);
                return "redirect:/admin/books";
            } catch (Exception e) {
                return "redirect:/admin/books";
            }
        }
        return "redirect:/auth/login";
    }

    @DeleteMapping("/books/{id}")
    public String deleteBook(@PathVariable Integer id, HttpSession session) {
        User user = (User) session.getAttribute("authUser");
        if (user != null && user.getRole_id() == 1) {
            bookService.deleteBook(id);
            return "redirect:/admin/books";
        }
        return "redirect:/auth/login";
    }
}
