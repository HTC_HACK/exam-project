package uz.pdp.controller;


//Asadbek Xalimjonov 2/25/22 8:47 AM


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {

    @GetMapping
    public String getHomePage()
    {
        return "home";
    }
}
