package uz.pdp.controller.auth;


//Asadbek Xalimjonov 2/25/22 9:05 AM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.model.User;
import uz.pdp.service.AuthService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/auth")
public class AuthController {


    @Autowired
    AuthService authService;

    @GetMapping("/login")
    public String getLoginPage(Model model) {

        return "auth/login";
    }


    @PostMapping("/login")
    public String getUserDetail(@RequestParam String username, @RequestParam String password, Model model, HttpSession session) {

        User user = authService.getUser(username, password);
        if (user != null) {
            session.setAttribute("authUser", user);
            switch (user.getRole_id()) {
                case 1:
                    return "redirect:/admin";
            }
        }

        model.addAttribute("error", "Email or Password Incorrect");
        return "/auth/login";
    }

    @GetMapping("/logout")
    public String getLogoutPage(HttpSession session) {
        session.invalidate();
        return "/auth/login";
    }
}
