package uz.pdp.service;


//Asadbek Xalimjonov 2/25/22 9:10 AM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.dao.AuthDao;
import uz.pdp.model.User;

import java.util.List;

@Service
public class AuthService {


    @Autowired
    AuthDao authDao;


    public User getUser(String username, String password) {
        return authDao.getUser(username, password);
    }

    public List<User> getAuthor() {
        return authDao.getAuthor();
    }

    public List<User> getBookAuthor(Integer id) {
        return authDao.getBookAuthor(id);
    }
}
