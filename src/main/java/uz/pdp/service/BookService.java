package uz.pdp.service;


//Asadbek Xalimjonov 2/25/22 9:38 AM


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.dao.BookDao;
import uz.pdp.model.Book;
import uz.pdp.model.User;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;


    public List<Book> getAllBooks() {

        return bookDao.getAllBooks();
    }


    public boolean deleteBook(Integer id) {
        return bookDao.deleteBook(id);
    }

    public boolean saveBook(Book book, List<User> users) {
        return bookDao.saveBook(book,users);
    }

    public Book getBookById(Integer id) {
        return bookDao.getBookById(id);
    }

    public boolean updateBook(Integer id,Book book,List<User> users) {
        return bookDao.updateBook(id,book,users);
    }
}
