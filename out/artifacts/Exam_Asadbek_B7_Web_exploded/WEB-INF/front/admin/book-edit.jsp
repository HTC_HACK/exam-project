<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/9/22
  Time: 7:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${authUser.username}</title>
    <%@ include file="../layouts/link.html" %>
</head>
<body>
<%@ include file="../layouts/menu.jsp" %>


<div class="container" style="padding-top: 3%;">

    <div class="row">
        <div class="col-md-6"><a href="/admin/books" class="btn btn-warning"
                                 style="margin-left: 16px; float: left; margin-bottom: 2rem">
            <i class="fas fa-arrow-left"></i> Back
        </a></div>

        <div class="col-md-6">
            <br>
            <form action="/admin/books/edit/${book.id}" method="post">
                <div class="form-group">
                    <input class="form-control" type="text" name="name" value="${book.name}" placeholder="Book name"
                           required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" type="text" name="description" placeholder="Book description"
                              required>${book.description}</textarea>
                </div>
                <c:if test="${bookAuthors.size()>0}">
                    <select class="form-control" name="authors" multiple>

                        <c:forEach items="${bookAuthors}" var="author">
                            <option value="${author.id}" selected>${author.username}</option>
                        </c:forEach>
                        <c:forEach items="${authors}" var="user">
                            <option value="${user.id}">${user.username}</option>
                        </c:forEach>
                    </select>
                </c:if>
                <c:if test="${bookAuthors.size()<1}">
                    <select class="form-control" name="authors" multiple>
                        <c:forEach items="${authorList}" var="author">
                            <option value="${author.id}">${author.username}</option>
                        </c:forEach>
                    </select>
                </c:if>
                <br>
                <button type="submit" class="btn btn-info"><i class="fas fa-edit"></i> Edit Book</button>
            </form>
        </div>
    </div>
</div>
<%@ include file="../layouts/scripts.html" %>
</body>
</html>
