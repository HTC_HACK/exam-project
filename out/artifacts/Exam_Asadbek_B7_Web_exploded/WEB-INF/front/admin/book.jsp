<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/9/22
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${authUser.username}</title>
    <%@ include file="../layouts/link.html" %>
</head>
<body>
<%@ include file="../layouts/menu.jsp" %>
<div class="container" style="padding-top: 2.6%;">
    <div class="row">
        <div class="col-md-12">
            <a href='/admin/books/create' class="btn btn-primary"
               style="float: right; margin-bottom: 2rem;margin-top: -1rem">
                <i class="fas fa-plus"></i> Add Book</a>
            <br>
            <table class="table table-bordered" style="text-align: center;background: white">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">View</th>
                    <th scope="col" colspan="2">Settings</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${bookList.size()<1}">
                    <tr>
                        <td colspan="5">No books</td>
                    </tr>
                </c:if>
                <c:forEach items="${bookList}" var="book">
                    <tr>
                        <td>${book.id}</td>
                        <td>${book.name}</td>
                        <td><a href="/admin/books/show/${book.id}" class="btn btn-info"><i class="fas fa-eye"></i></a></td>
                        <td><a href="/admin/books/edit/${book.id}" class="btn btn-success"><i class="fas fa-edit"></i> </a></td>
                        <td>
                            <button onclick="deleteCourse('books/',${book.id})"
                                    class="btn btn-danger" type="button">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                <tbody></tbody>
                <tbody></tbody>
            </table>
        </div>

    </div>
</div>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    function deleteCourse(param2, id) {
        fetch(param2 + id, {
            method: "DELETE"
        }).then(response => location.reload());
    }
</script>
<%@ include file="../layouts/scripts.html" %>
</body>
</html>
