<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-lg navbar-light" style="background: #fff">
    <div class="container" style="padding-right: 7%;padding-left: 7%">
        <a class="navbar-brand d-flex align-items-center" href="/">

        </a>
        <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse"
                data-target="#navbar4">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar4">

            <ul class="navbar-nav mr-auto pl-lg-4" style="float: right">
            </ul>
            <ul class="navbar-nav ml-auto mt-6 mt-lg-0">
                <li class="nav-item"><a class="nav-link" href="/">
                    <i class="fas fa-home icon"></i> Home
                </a></li>
                <c:if test="${authUser!=null && authUser.role_id==1}">
                    <li class="nav-item"><a class="nav-link" href="/admin">
                        <i class="fas fa-user icon"></i> ${authUser.username}
                    </a></li>
                    <li class="nav-item"><a class="nav-link" href="/admin/books">
                        <i class="fas fa-books icon"></i> Books
                    </a></li>
                    <li class="nav-item"><a class="nav-link" href="/auth/logout">
                        <i class="fas fa-sign-in-alt icon"></i> Logout
                    </a></li>
                </c:if>
                <c:if test="${authUser==null}">
                    <li class="nav-item"><a class="nav-link" href="/auth/login">
                        <i class="fas fa-sign-in-alt icon"></i> Sign In
                    </a></li>
                </c:if>
            </ul>
        </div>

    </div>
</nav>
